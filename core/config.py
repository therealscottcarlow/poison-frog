import inspect
import json
import os


def save(conf):
    json.dump(conf, open('config', 'w'), sort_keys=True, indent=1)

if not os.path.exists('config'):
    open('config', 'w').write(inspect.cleandoc(
r'''{
 "username":'username',
 "password":'password',
 "admin_overrides_ignores": true,
 "force_join_on_start_up": true,
 "no_ignores_print_to_console": true,
 "prefix":',',
 "censored_strings":[],
 "group_order":["bot owner","super admins","admins","mods","voiced"],
 "ignored":{
   "usernames":[],
   "user_ids":[],
   "channels":[]
 },
 "API_keys":{
  "youtube":""
 },
 "groups":{
   "admins":["97147368410976256"]
 },
 "servers":{
   "example":"https://discord.gg/<invite_ID>"
 },
 "ignored":["*bot"]
}''') + '\n')

