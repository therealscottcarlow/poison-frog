import re


@hook.cmd
def help(inp,message=None):
    "help [command] -- gives a list of commands/help for a command"

    funcs = {}
    disabled = bot.config.get('disabled_plugins', [])
    disabled_comm = bot.config.get('disabled_commands', [])

    commands = hook.cmds
    if not inp:
        length = 0
        out = ["", "", "", ""]
        well = []
        for x in commands:
            well.append(x)
        well.sort()
        out[0] += "available commands:"
        for x in well:
            if len(out[0]) + len(str(x)) < 1000:
                out[0] += " " + str(x)
            else:
                if len(out[0]) + len(str(x)) < 1000:
                    out[1] += " " + str(x)
                else:
                    if len(out[1]) + len(str(x)) < 1000:
                        out[2] += " " + str(x)
                    else:
                        if len(out[2]) + len(str(x)) < 1000:
                            out[3] += " " + str(x)

        client.send_message(message.channel,'```'+out[0][0:]+'```')
        if out[1]:
            client.send_message(message.channel,'```'+out[1][1:]+'```')
        if out[2]:
            client.send_message(message.channel,'```'+out[2][1:]+'```')
        if out[3]:
            client.send_message(message.channel,'```'+out[3][1:]+'```')
    else:
        if inp in commands:
            response = commands[inp].__doc__
            if not response==None:
                client.send_message(message.channel,'```'+bot.config["prefix"]+response+'```')
            else:
                client.send_message(message.channel,'No help doc for '+inp)




