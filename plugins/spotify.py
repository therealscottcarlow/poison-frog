import requests,re
import spotipy


gateway = 'http://open.spotify.com/{}/{}'  # http spotify gw address
spuri = 'spotify:{}:{}'
spotify_re = (r'(play\.spotify\.com\/|spotify\:)(track|album|artist|user)[/|:]([a-zA-Z0-9]+)', re.I)



@hook.regex(*spotify_re)
def spotify_url(match,message=None):
    _type = match.group(2)
    spotify_id = match.group(3)
    url = spuri.format(_type, spotify_id)
    # no error catching here, if the API is down fail silently
    sp = spotipy.Spotify()
    track = sp.track(url)
    
    art = track['album']['images'][0]['url']
    album = track['album']['name']
    track_name = track['name']
    #artist = track["album"]["artists"][0]["name"]
    track_url = track['external_urls']['spotify']
    artist = ""
    return(url+"\n"+track_name+"\nFrom "+album+"( "+art+" )\nURL: "+track_url)
