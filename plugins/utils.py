
@hook.cmd
def resolve_username(input):
    "resolve_username <input> -- Returns the ID of the first person who has <input> as their username."
    return(bot.resolve_username(input).id)

@hook.cmd
def reverse(input):
    "reverse <input> -- Reverses the input you give it."
    bot.clean_message = False
    return(u"\u202E"+unicode(input)+u"\u202D",True)

@hook.cmd
def my_id(input,message=None):
    "my_id -- retruns your discord ID."
    return(message.author.id)

@hook.cmd
def serverinfo(input,message=None):
    "serverinfo -- Returns information about the current server."
    response = '```'
    server = message.channel.server
    role_array = []
    for role in server.roles:
        role_array.append(role.name)
    roles = ", ".join(role_array)
    roles = roles.replace("@","@\x00")
    response = response+"ID: "+server.id+"\nName: "+server.name+"\nOwner: ("+server.owner.id+")"+server.owner.name+"\nRegion: "+server.region+"\nAvatar: "+server.icon+"\nAvatar URL: https://cdn.discordapp.com/icons/"+server.id+"/"+server.icon+".jpg"+"\nUsers: "+str(len(server.members))+"\nRoles: "+roles+'```'
    return(response)


@hook.cmd
def userinfo(input,message=None):
    "userinfo <user> -- Returns user information of <user>."
    response = None
    mentions = bot.mentions(input)
    if len(mentions)>0:
        response = "```"
        if len(mentions)>0:
            for data in mentions:
                id = data.id
                name = data.name
                disc = str(data.discriminator)
                avatar_url = data.avatar_url()
                avatar_hash = data.avatar
                response = response+"ID: "+id+"\nDiscriminator: "+disc+"\nName: "+name+"\nAvatar: "+avatar_hash+"\nAvatar URL: "+avatar_url+"\n\n"
    else:
        response = "```"
        data = message.author
        response = response+"ID: "+data.id+"\nDiscriminator: "+str(data.discriminator)+"\nName: "+data.name+"\nAvatar: "+data.avatar+"\nAvatar URL: "+data.avatar_url()+"\n\n"
    if not response==None:
        response = response+"```"
    return(response)

@hook.cmd
def botinfo(input):
    "botinfo -- Returns this bot's discord information."
    data = client.user
    response = "```ID: "+data.id+"\nDiscriminator: "+str(data.discriminator)+"\nName: "+data.name+"\nAvatar: "+data.avatar+"\nAvatar URL: "+data.avatar_url()+"```"
    return(response)

@hook.event("discord.gg/","discordapp.com/invite/")
def auto_invite(input,message=None):
    if bot.permission_check(message.author,bot.get_highest_group()):
        if "discord.gg/" in input:
            invite_split = message.content.split("discord.gg/")[1].split(" ")[0]
        if "discordapp.com/invite/" in input:
            invite_split = message.content.split("discordapp.com/invite/")[1].split(" ")[0]
        invite = "https://discord.gg/"+invite_split
        old_servers = len(client.servers)
        client.accept_invite(invite)
        if len(client.servers)>old_servers:
            bot.config['servers'] = {}
            for data in client.servers:
                try:
                    bot.config['servers'][data.name] = client.create_invite(data).url
                except:
                    bot.config['servers'][data.name] = invite
            bot.save_config()
            return(bot.mention(message.author)+": Joined that server")

@hook.cmd
def version(input):
    return("Alpha 1.0")

