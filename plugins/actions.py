
import random

@hook.event("!kick")
def lol_kick(input,message=None):
    return("I can't do that Dave")

@hook.event("*")
def action(input,message=None):
    if input.startswith("*") and input.endswith("*"):
        input = input[1:-1]
        split = input.split(" ")
        split_lower = []
        response = ["*Croaks*","*Ribbits*"]
        for data in split:
            split_lower.append(data.lower())
        if client.user.name.lower() in split_lower or bot.mention(client.user) in split:
            return(random.choice(response))

@hook.event(client.user.name,client.user.name.lower(),bot.mention(client.user))
def return_hi(input,message=None):
    if input.lower().startswith("hi "):
        return("Hi <@"+message.author.id+">")